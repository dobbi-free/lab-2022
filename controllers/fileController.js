import fs from "fs";
import path from "path";

const filesDirPath = "./files/";

export const createFile = (req, res) => {
  try {
    const { filename, content, type, password } = req.body;

    if (filename && content) {
      fs.appendFile(`${filesDirPath}${filename}${type}`, `${content}`, (err) => {
        if (err) {
          return res.status(500).json({ message: `${err}` });
        }
      });

      if (password) {
        const passwords = JSON.parse(fs.readFileSync("./passwords.json"));
        passwords[filename] = password;
        fs.writeFileSync("./passwords.json", JSON.stringify(passwords));
      }
    } else {
      return res.status(400).send({ message: "Please specify parameters" });
    }
    return res.status(200).send({ message: "File created successfully" });
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};

export const getFiles = (req, res) => {
  try {
    const response = fs.readdirSync(filesDirPath);

    return res.status(200).json({
      message: "Success",
      files: [...response],
    });
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};

export const getFile = (req, res) => {
  try {
    const { filename } = req.params;

    const file = fs
      .readdirSync(`${filesDirPath}`)
      .find((el) => el === filename);
    const fileContent = fs.readFileSync(`${filesDirPath}${file}`, "utf-8");
    const fileExt = path.extname(file);
    const uploadedDate = fs.statSync(`${filesDirPath}${file}`).birthtime;

    if (file) {
      return res.status(200).send({
        message: "Success",
        filename: file,
        content: fileContent,
        extension: fileExt,
        uploadedDate: uploadedDate,
      });
    } else {
      return res
        .status(404)
        .send({ message: `No file with ${filename} filename found` });
    }
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};

export const changeFile = (req, res) => {
  try {
    const { filename } = req.params;

    const { content } = req.body;

    const file = fs
      .readdirSync(`${filesDirPath}`)
      .find((el) => el === filename);

    if (file) {
      fs.writeFileSync(`${filesDirPath}/${file}`, content);

      return res.status(200).send({
        message: `${file} has been updated`,
      });
    } else {
      return res
        .status(404)
        .send({ message: `No file with ${filename} filename found` });
    }
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};

export const deleteFile = (req, res) => {
  try {
    const { filename } = req.params;

    const file = fs
      .readdirSync(`${filesDirPath}`)
      .find((el) => el === filename);

    if (file) {
      fs.unlinkSync(`${filesDirPath}/${file}`);
      // remove password
      const passwords = JSON.parse(fs.readFileSync("./passwords.json"));
      delete passwords[filename];
      fs.writeFileSync("./passwords.json", JSON.stringify(passwords));

      return res.status(200).send({
        message: `${file} has been deleted`,
      });
    } else {
      return res
        .status(404)
        .send({ message: `No file with ${filename} filename found` });
    }
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};
