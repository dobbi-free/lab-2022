import File from "../model/fileModel.js";

export const createFile = async (req, res) => {
  try {
    const { title, text } = req.body;
    const { _id } = req.user;

    if (title && text) {
      const file = await File.create({ title, text, authorId: _id });

      if (file) {
        return res.status(200).send({ message: "File created successfully" });
      }
    } else {
      return res.status(400).send({ message: "Please specify parameters" });
    }
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};

export const getFiles = async (req, res) => {
  try {
    const { _id } = req.user;

    const files = await File.find({ author: _id });

    return res.status(200).json(files);
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};

export const getFile = async (req, res) => {
  try {
    const { id } = req.params;

    const file = await File.findById(id);

    if (file) {
      return res.status(200).send(file);
    } else {
      return res.status(404).send({ message: `File not found` });
    }
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};

export const changeFile = async (req, res) => {
  try {
    const { id } = req.params;

    const { text, title } = req.body;

    const file = await File.findByIdAndUpdate(id, { text, title });
    if (file) {
      return res.status(200).send({
        message: `File has been updated`,
      });
    } else {
      return res.status(404).send({ message: `File not found` });
    }
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};

export const deleteFile = async (req, res) => {
  try {
    const { id } = req.params;

    await File.findByIdAndDelete(id);

    return res.status(200).send({
      message: `File has been deleted`,
    });
  } catch (e) {
    return res.status(500).send({ message: `${e}` });
  }
};
