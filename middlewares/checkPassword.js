import fs from "fs";

export const checkPassword = (req, res, next) => {
  const passwords = JSON.parse(fs.readFileSync("./passwords.json"));
  const { filename } = req.params;
  const { password } = req.body;

  if (passwords[filename]) {
    if (passwords[filename] !== password) {
      return res.status(403).json({ message: "Invalid password" });
    }
  }

  next();
};
