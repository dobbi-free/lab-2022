import mongoose from "mongoose";

const fileSchema = mongoose.Schema(
  {
    authorId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    title: {
      type: String,
      required: [true, "Please add a title"],
    },
    text: {
      type: String,
      required: [true, "Please add a name"],
    },
  },
  {
    timestamp: true,
  }
);

export default mongoose.model("File", fileSchema);
