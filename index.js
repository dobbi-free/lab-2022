import express from "express";
import morgan from "morgan";
import fileRouter from "./routes/fileRouter.js";
import fileV2Router from "./routes/fileV2Router.js";
import userRouter from "./routes/userRouter.js";
import bodyParser from "body-parser";
import { FILE_CONTROLLER_ROUTE } from "./constants/fileConstants.js";
import { FILE_V2_CONTROLLER_ROUTE } from "./constants/fileV2Constants.js";
import { USER_CONTROLLER_ROUTE } from "./constants/userConstants.js";
import cors from "cors";
import { connectDB } from "./config/db.js";
import dotenv from "dotenv";

dotenv.config();

connectDB();

const app = express();

app.use(cors());

app.use(morgan("tiny"));

app.use(bodyParser.json());

app.use(FILE_CONTROLLER_ROUTE, fileRouter);
app.use(FILE_V2_CONTROLLER_ROUTE, fileV2Router);
app.use(USER_CONTROLLER_ROUTE, userRouter);

app.listen(8080, () => {
  console.log(`Server:8080`);
});
