export const FILE_CONTROLLER_ROUTE = "/api/v1/files";
export const FILE_GET_ALL_ENDPOINT = "/";
export const FILE_CREATE_ENDPOINT = "/";
export const FILE_CHANGE_ENDPOINT = "/:filename";
export const FILE_GET_ONE_ENDPOINT = "/:filename";
export const FILE_DELETE_ENDPOINT = "/:filename";
