export const USER_CONTROLLER_ROUTE = "/api/v1/user";
export const USER_REGISTER_ENDPOINT = "/register";
export const USER_LOGIN_ENDPOINT = "/login";
export const USER_GET_INFO_ENDPOINT = "/";

