export const FILE_V2_CONTROLLER_ROUTE = "/api/v2/files";
export const FILE_GET_ALL_ENDPOINT = "/";
export const FILE_CREATE_ENDPOINT = "/";
export const FILE_CHANGE_ENDPOINT = "/:id";
export const FILE_GET_ONE_ENDPOINT = "/:id";
export const FILE_DELETE_ENDPOINT = "/:id";
