import express from "express";
import {
  createFile,
  getFiles,
  getFile,
  changeFile,
  deleteFile,
} from "../controllers/fileV2Controller.js";
import { protect } from "../middlewares/auth.js";
import {
  FILE_CREATE_ENDPOINT,
  FILE_GET_ALL_ENDPOINT,
  FILE_GET_ONE_ENDPOINT,
  FILE_CHANGE_ENDPOINT,
  FILE_DELETE_ENDPOINT,
} from "../constants/fileV2Constants.js";

const router = express.Router();

router.get(FILE_GET_ALL_ENDPOINT, protect, getFiles);
router.get(FILE_GET_ONE_ENDPOINT, getFile);
router.put(FILE_CHANGE_ENDPOINT, protect, changeFile);
router.delete(FILE_DELETE_ENDPOINT, protect, deleteFile);
router.post(FILE_CREATE_ENDPOINT, protect, createFile);

export default router;
