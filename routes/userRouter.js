import express from "express";
import { registerUser, loginUser, getUser } from "../controllers/userController.js";
import {
  USER_REGISTER_ENDPOINT,
  USER_LOGIN_ENDPOINT,
  USER_GET_INFO_ENDPOINT,
} from "../constants/userConstants.js";

const router = express.Router();

router.post(USER_REGISTER_ENDPOINT, registerUser);
router.post(USER_LOGIN_ENDPOINT, loginUser);
router.get(USER_GET_INFO_ENDPOINT, getUser);

export default router;
