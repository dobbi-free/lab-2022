import express from "express";
import {
  createFile,
  getFiles,
  getFile,
  changeFile,
  deleteFile,
} from "../controllers/fileController.js";
import { checkPassword } from "../middlewares/checkPassword.js";
import { protect } from "../middlewares/auth.js";
import {
  FILE_CREATE_ENDPOINT,
  FILE_GET_ALL_ENDPOINT,
  FILE_GET_ONE_ENDPOINT,
  FILE_CHANGE_ENDPOINT,
  FILE_DELETE_ENDPOINT,
} from "../constants/fileConstants.js";

const router = express.Router();

router.get(FILE_GET_ALL_ENDPOINT, getFiles);
router.get(FILE_GET_ONE_ENDPOINT, checkPassword, getFile);
router.put(FILE_CHANGE_ENDPOINT, protect, checkPassword, changeFile);
router.delete(FILE_DELETE_ENDPOINT, protect, checkPassword, deleteFile);
router.post(FILE_CREATE_ENDPOINT, protect, createFile);

export default router;
